#ifndef HEADER_FILE
#define HEADER_FILE
#include <complex.h>

int get_num_jammers();
int get_num_signals();
void get_jam_signal(double complex *jam);
void get_tgt_signal(double complex *tgtsig);
void get_noise(double complex *noise_sig);
void get_jn(double complex *jn,double complex *jam);

#endif
