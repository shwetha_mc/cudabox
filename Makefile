CC=gcc
HDRFILES=include/init.h include/setup.h
LDFLAGS=-lm -lrt -lpthread 
CFLAGS=-std=c99 -O2 -std=gnu++0x
HEADERS= $(HDRFILES)
SOURCEDIR=src/
BINDIR=bin/
LIBRARIES=/home/shwetha_mc/adbf2/lib/libevpath.so.3 /home/shwetha_mc/adbf2/lib/libffs.so.1 /home/shwetha_mc/adbf2/lib/libatl.so.2 /home/shwetha_mc/adbf2/lib/libdill.so.1 /home/shwetha_mc/adbf2/lib/libcercs_env.so.1
HEADERIMP=src/init.c src/setup.c
BNODE=bnode
CNODE=covariance

all: $(CNODE) $(BNODE)

$(BNODE): 
	$(CC) -o $(BINDIR)$@ $(SOURCEDIR)$@.c $(HEADERS) $(HEADERIMP) $(LDFLAGS) $(LIBRARIES)

$(CNODE):
	$(CC) -o $(BINDIR)$@ $(SOURCEDIR)$@.c $(HEADERS) $(HEADERIMP) $(LDFLAGS) $(LIBRARIES)

.PHONY: clean

clean:
	rm -f bin/bnode bin/covariance bin/setup bin/init .o *~
