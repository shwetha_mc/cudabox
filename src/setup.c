#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
#include<math.h>
#include "../include/init.h"

#define NX 16
#define NY 16

/*typedef struct 
{
	double complex sig[NX][NY];
}typ_signal;
*/
int get_num_jammers()
{
	 int jamnum=0;
	 //char flag;
	char *line=0;
	 size_t size=0;
	 FILE *conf;
	//printf("count1\n");
	 conf=fopen("../sig_config.txt","r");
	 if(conf==NULL)
	 printf("Error");
//	printf("canitbe?");
	
while(getline(&line,&size,conf)>0)
         {
                if(*(line)=='j')
                ++jamnum;
         }
  fclose(conf);



/*	while((fscanf(conf,"%c %*f %*f %*f ",&flag)==4))
	// while(getline(&line,&size,conf)>0)
	 {
		//printf("countinc\n");
		if(flag=='j')
		++jam;
	 }
	 fclose(conf);
*/
	 return jamnum;
//	fclose(conf);

}

int get_num_signals()
{
	 int sig=0;
         char *line=0;
         size_t size=0;
         FILE *conf;
         conf=fopen("../sig_config.txt","r");
         if(conf==NULL)
         printf("Error");
         while(getline(&line,&size,conf)>0)
         {
                if(*(line)=='s')
                ++sig;
         }
  fclose(conf);	  
         return sig;
      
}

void get_jam_signal(double complex *jam)
{
	char flag;
	//printf("pre coint\n");
	int n=get_num_jammers();
	//printf("after count");
	int i=0,j=0,p=0,k=0;
	float amp,az,el;
	double complex jamsum[NX][NY];
	for(k=0;k<NX;++k)
         {
                for(p=0;p<NY;++p)
		jamsum[k][p]=0;
	}
	//typ_signal jamarr[n];
	FILE *conf;
//	printf("file?\n");
	conf=fopen("../sig_config.txt","r");
	if(conf==NULL)
       printf("Error");
	//printf("n=%d",n);
//	printf("open\n");
	while(i<n && (fscanf(conf,"%c %f %f %f ",&flag,&amp,&az,&el)==4))
        {
//		printf("Inside while\n");	
	       if(flag=='j')
		{
//		printf("%f %f %f",az,el,amp);
		signalcomp(jam,amp,az,el);
//		printf("computed sig\n");
		for(k=0;k<NX;++k)
                {
                for(p=0;p<NY;++p)
		{
		jamsum[k][p]+=*(jam+(k*NY+p));
//		printf("%f+i%f\t",creal(*(jam+k*NY+p)),cimag(*(jam+k*NY+p)));
		}
		}
				
		++i;
		}
	}
	//printf("BP\n");
	for(i=0;i<n;++i)
	{
		for(k=0;k<NX;++k)
        	{
                for(p=0;p<NY;++p)
	        *(jam+(k*NY+p))=jamsum[k][p];
		}
	}
/*	for(i=0;i<NX;++i)
        {
                for(j=0;j<NY;++j)
                printf("%f+i%f",creal(jamsum[i][j]),cimag(jamsum[i][j]));
                printf("\nline\n");
        }
*/
	fclose(conf);	
}

void get_tgt_signal(double complex *tgtsig)
{
	char flag;
        //int n=get_num_jammers();
        int i=0,p=0,k=0;
        float amp,az,el;
	
        //typ_signal jamarr[n];
        FILE *conf;
        conf=fopen("../sig_config.txt","r");
        if(conf==NULL)
        printf("Error");
	while(fscanf(conf,"%c %f %f %f ",&flag,&amp,&az,&el)==4)
	{
        	if(flag=='s')
		{
		signalcomp(tgtsig,amp,az,el);
		}
	}
	fclose(conf);
}


void get_noise(double complex *noise_sig)
{
	double randna[NX][NY], randnb[NX][NY];
	char flag;
	float amp;
	FILE *conf;
	conf=fopen("../sig_config.txt","r");
        if(conf==NULL)
        printf("Error");
	while(fscanf(conf,"%c %f %*f %*f ",&flag,&amp)==2)
        {
                if(flag=='n')
                {
		break;
               }
        }
	//printf("noiseamp=%f\n",amp);
	int i,j;
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		randna[i][j]=pseudorandn();
		randnb[i][j]=pseudorandn();
		}
	}
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		*(noise_sig+(i*NY+j))=sqrt(0.5)*((randna[i][j]*amp)+(I*randnb[i][j]*amp));
		}
	}
	fclose(conf);
}


void get_jn(double complex *jn, double complex *jam)
{
	int i,j;
	double complex noise[NX][NY];
	 for(i=0;i<NX;++i)
        {
           for(j=0;j<NY;++j)
           {
         //       jam[i][j]=0;
		noise[i][j]=0;
          }
        }

	//get_jam_signal(&jam);
	get_noise(&noise);
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
	  {
		*(jn+(i*NY+j))=(*(jam+(i*NY+j)))+noise[i][j];

//pseudorandn()*
//(*(jam+(i*NY+j)));
//+noise[i][j];
//*(jam+(i*NY+j));
//pseudorandn()*(*(jam+(i*NY+j)))+noise[i][j];
	  }
	}
}


void calc_inverse(double complex *Rinv)
{
	int nxy=NX*NY;
	int i,j,k;
	double complex iden[nxy][nxy];
	//printf("here\n");
	double arr[nxy][nxy];
//	printf("here\n");
	double complex factor;
	for(i=0;i<nxy;i++)
	{
		for(j=0;j<nxy;++j)
		arr[i][j]=*(Rinv+(i*nxy)+j);
	}
	/*Identity matrix*/
	for(i=0;i<nxy;++i)
        {
	        for(j=0;j<nxy;++j)
        	    	iden[i][j]=(i==j)?(1+I):0;
	}
	/*augmenting base matrix with identity*/
	for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        arr[i][j+nxy]=iden[i][j];
                }
        }
	/*Gaussian decomposition*/
	for(k=0;k<nxy;++k)
        {
        factor=arr[k][k];
        for(i=0;i<2*nxy;++i)
        {
                arr[k][i]/=factor;
        }
        for(i=0;i<nxy;++i)
        {
                if(i!=k)
                {
                factor=arr[i][k];
                for(j=0;j<2*nxy;++j)
                {
                        arr[i][j]=arr[i][j]-(arr[k][j]*factor);

                }
                }
        }
        }
	/*assignment*/
	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
			*(Rinv+(i*nxy+j))=arr[i][j+nxy];
		}
	}


	
	
}



/*


void main()
{
	printf("%d jammers\n",get_num_jammers());
	printf("%d signals\n",get_num_signals());
	int i=0,j=0;
	double complex jam[NX][NY],tgtsig[NX][NY],noisesig[NX][NY],jn[NX][NY];
	
	for(;i<NX;++i)
	{
	   for(j=0;j<NY;++j)
	   {
		jam[i][j]=0;
		tgtsig[i][j]=0;
		noisesig[i][j]=0;
		jn[i][j]=0;
	   }
	}
	get_jam_signal(&jam);
	printf("Jammer cumulative is\n");
	for(i=0;i<NX;++i)
	{
	   for(j=0;j<NY;++j)
		printf("%f+i%f\t",creal(jam[i][j]),cimag(jam[i][j]));
	  printf("line\n");
	}
	printf("Target \n");
	get_tgt_signal(&tgtsig);	
	for(i=0;i<NX;++i)
        {
           for(j=0;j<NY;++j)
                printf("%f+i%f\t",creal(tgtsig[i][j]),cimag(tgtsig[i][j]));
          printf("\nline\n");
        }
	 printf("Noise \n");
        get_noise(&noisesig);
        for(i=0;i<NX;++i)
        {
           for(j=0;j<NY;++j)
                printf("%f+i%f\t",creal(noisesig[i][j]),cimag(noisesig[i][j]));
          printf("\nline\n");
        }
	printf("jam and noise \n");
	get_jn(&jn,jam,noisesig);
	for(i=0;i<NX;++i)
        {
           for(j=0;j<NY;++j)
                printf("%f+i%f\t",creal(jn[i][j]),cimag(jn[i][j]));
          printf("\nline\n");
        }
	

}
*/
