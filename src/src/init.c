#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<complex.h>
#include<time.h>

#define _XOPEN_SOURCE
#define SPD_OF_LIGHT 2.99e8
#define FREQ 10e9
#define NX 16
#define NY 16
#define PI 3.14159265
#define LAMBDA ceilf(SPD_OF_LIGHT/FREQ*1000)/1000

double pseudorandn()
{
	int i;
	unsigned short int x16v[3];
	double sum=0;
	//srand48((unsigned)time(NULL));
	for(i=0;i<32;++i)
		sum+=(double)mrand48()/(double)RAND_MAX;
	return (sum/32);
}



void initspace(double* x, double* y)
{
  int i;
  for(i=0;i<NY;++i)
	{
	  *(x+i)=(((LAMBDA/2))*(i));

	}
   for(i=0;i<NX;++i)
	*(y+i)=(((LAMBDA/2))*(i));
}

void steering_vector(double complex* sv, double x[], double y[], double az, double el)
{
	double complex svx[NY],pow[NY],pow2[NX];
	double complex svy[NX];
	double sine,sine2;
	int i, j;
	for(i=0;i<NY;++i)
	{	
		sine=sin(az*PI/180);
		pow[i]=2*PI*x[i]*I*1000000/LAMBDA;
		svx[i]=cexp(pow[i]*sine);		
//		svx[i]=ceil(creal(cexp(pow[i]*sine))*10000)/10000+(I*(ceil(cimag(cexp(pow[i]*sine))*10000)/10000));
      
	}
	for(j=0;j<NX;++j)
	{
		sine2=sin(el*PI/180);
        	pow2[j]=2*PI*y[j]*I*1000000/LAMBDA;
		  svy[j]=cexp(pow2[j]*sine2);
//		svy[j]=ceil(creal(cexp(pow2[j]*sine2))*10000)/10000+(I*(ceil(cimag(cexp(pow2[j]*sine2))*10000)/10000));
	}
	/*Transposing svy*/
	/*for(k=0;k<NX;++k)
	{
	for(i=1;i<NY;++i)
	{
	svy[k][0]=svy_pr[k];
	svy[k][i]=0;
	}
	}

	 printf("svy:\n");
	for(p=0;p<NX;++p)
	printf("%f+i%f \t",creal(svy[p][0]),cimag(svy[p][0]));
	*/
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
	  {
		*(sv+(i*NX+j))+=svy[i]*svx[j];
	  }
	}
	
	
	/*printf("SV\n");
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
    	  {
		printf("%f+i%f\t",creal(*(sv+(i*NX+j))),cimag(*(sv+(i*NX+j))));
	  }
	}*/
		
}	

void signalcomp(double complex* sig,  double amp, double az, double el)
{

	double x[NY], y[NX];
	initspace(&x,&y);
   	//double _Complex signal[NX][NY];
	double complex sv[NX][NY];
	int s,w;
	for(s=0;s<NX;++s)
	{
	  for(w=0;w<NY;++w)
	  {
		sv[s][w]=0;
	  }
	}
		steering_vector(&sv,x,y,az,el);
	
	for(s=0;s<NX;++s)
	{
	  for(w=0;w<NY;++w)
	  {
	 	*(sig+(s*NX+w))=amp*sv[s][w];	
	  }
	}

	//return signal;
}

/*
int main()
{
int i,j;
double x[NX];
double y[NY];
initspace(&x,&y);
for(i=0;i<NX;++i)
printf("%f\t",*(x+i));
printf("\n");
for(i=0;i<NY;++i)
printf("%f\t",*(y+i));
printf("\nEnd of x y \n");
double complex jam1[NX][NY];
signalcomp(&jam1,100,-30,-15,x,y);
for(i=0;i<NX;++i)
{
for(j=0;j<NY;++j)

printf("%g+i%g\t",jam1[i][j]);
//printf("%d+i%d\t",creal(jam1[i][j]),cimag(jam1[i][j]));
printf("\nline\n");
}

}
*/
