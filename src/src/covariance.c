#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
#include<string.h>
#include<arpa/inet.h>
#include<time.h>
#include "../include/setup.h"
#include "../../include/evpath.h"
#include "../../include/atl.h"
#define NX 16
#define NY 16
#define SIGRANGEBIN 50
//#define RANGEBINS DEFAULT 1000
#define NXY NX*NY
#define DEST_PORT 26068 //26067 26068
#define DEST_IP "10.255.255.188"

struct timeval sent;
int rangebins;

typedef struct {
 double real;
 double imag;
}comp;

typedef struct _inverse {
    comp inv_data[NXY][NXY];
    comp raw_data[NX][NY];
} inverse, *inverse_ptr;
static FMField compfield_list[] =
{
    {"real", "float", sizeof(float), FMOffset(comp*, real)},
   {"imag", "float", sizeof(float), FMOffset(comp*, imag)},
    {NULL, NULL, 0, 0}
};

static FMField inverse_field_list[] =
{
  {"inv_data", "comp[256][256]", sizeof(comp), FMOffset(inverse*, inv_data)},
{"raw_data", "comp[16][16]", sizeof(comp), FMOffset(inverse*, raw_data)},
    {NULL, NULL, 0, 0}
};


static FMStructDescRec inverse_format_list[] =
{
    {"inverse", inverse_field_list, sizeof(inverse), NULL},
   {"comp", compfield_list, sizeof(comp), NULL},
    {NULL, NULL}
};

double complex mul_comp(double complex a, double complex b)
{
	return(((creal(a)*creal(b))-(cimag(a)*cimag(b)))+I*((creal(a)*cimag(b))+(cimag(a)*creal(b))));
}
double complex div_comp(double complex a, double complex b)
{
	 return((((creal(a)*creal(b))-(cimag(a)*cimag(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b))))+I*(((creal(a)*cimag(b))+(cimag(a)*creal(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b)))));
}



void vectorize(double complex *jnv, double complex jn[NX][NY])
{

	int i,j,k=0;
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
	  {
		//if(k<NXY)
		//{
		*(jnv+k)=jn[j][i];
//(round(creal(jn[i][j])*10000)/10000)+I*((round(cimag(jn[i][j])*10000)/10000));
		++k;
	//	}
		
	  }
	}
}

void calc_inverse(double complex *Rinv)
{
	//int nxy=2;
	int nxy=NX*NY;
        int i,j,k,temp;
        double complex iden[nxy][nxy];
        double complex arr[nxy][2*nxy];
        double complex r,swaptemp,factor;
        for(i=0;i<nxy;i++)
        {
                for(j=0;j<nxy;++j)
                arr[i][j]=*(Rinv+(i*nxy)+j);
        }
	
      /*Identity matrix*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                        iden[i][j]=(i==j)?(1):0;
        }
        /*augmenting base matrix with identity*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        arr[i][j+nxy]=iden[i][j];
                }
        }
        /*Gaussian decomposition*/
	for(k=0;k<nxy;++k)
        {
        factor=((arr[k][k]));
        for(i=0;i<2*nxy;++i)
        {
                arr[k][i]=(arr[k][i]/factor);
        }
        for(i=0;i<nxy;++i)
        {
                if(i!=k)
                {
                factor=((arr[i][k]));
                for(j=0;j<2*nxy;++j)
                {
                        arr[i][j]=arr[i][j]-(arr[k][j]*factor);

                }
                }
        }
        }

//	printf("\nends\n");
	 /*assignment*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        *(Rinv+(i*nxy+j))=arr[i][j+nxy];
                }
        }
	
	/* for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
		printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
		}
		printf("\nline\n");
	}*/
		
	
	
}

void calc_covariance(double complex *Rinv, double complex *raw_data)
{
	int m,i,j,k=0,nxy=NX*NY;
	double complex val=0;
	double complex jam[NX][NY],sig[NX][NY],jn[NX][NY],jncopy[NX][NY],jnv[nxy],R[nxy][nxy];
	int p,q;
	double complex inter[nxy][nxy];
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
			sig[i][j]=0;
                        jam[i][j]=0;
                        jn[i][j]=0;
			*(raw_data+(i*NY)+j)=0;


		}
	}
	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
			*(Rinv+(i*nxy)+j)=0;
			R[i][j]=0;
		}
	}
	get_jam_signal(&jam);
	get_tgt_signal(&sig);
	//get_jn(&jn,&jam);	
	
/*	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		jn[i][j]=jam[i][j];
		}
	}*/
	
	if(!rangebins)
	{
	FILE *rngbin;
	rngbin=fopen("../rangebins.txt","r");
	fscanf(rngbin,"%d \n",&rangebins);
	fclose(rngbin);
	}
	printf("Rangebins set : %d\n", rangebins);
	for(m=0;m<rangebins;++m)
	{
//	int w,v=0;
         get_jn(&jn,&jam);
	 if((m+1)==SIGRANGEBIN)
	 {
		for(i=0;i<NX;++i)
		{
			for(j=0;j<NY;++j)
			{
			jn[i][j]+=sig[i][j];
			*(raw_data+(i*NY+j))=jn[i][j];
			}
		}
	 }
//		printf("jnv\n");	
/*	for(i=0;i<NX;++i)
	{
	for(j=0;j<NY;++j)
	{
		jncopy[i][j]=jn[j][i];
	}
	}*/
	 vectorize(&jnv,jn);
	/*printf("jn\n");
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		printf("%f+i%f\t",creal(jn[i][j]),cimag(jn[i][j]));
		printf("\nline\n");
	}*/
	 
	for(p=0;p<nxy;++p)
	{
		for(q=0;q<nxy;++q)
		inter[p][q]=0;
	}

         for(i=0;i<nxy;++i)
	 {
		for(j=0;j<nxy;++j)
		{
		inter[i][j]=jnv[i]*(conj(jnv[j]));
		}
	}
	
	 for(p=0;p<nxy;++p)
	 {
		for(q=0;q<nxy;++q)
		{
		R[p][q]+=inter[p][q];
		}
	 }
/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
                printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
                }
                printf("\nline\n");
        }

*/	
	}
 	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		*(Rinv+(i*nxy)+j)=R[i][j];
		}
	}
/*	 for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
		{
		printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
		}
		printf("\nline\n");
	}*/
	calc_inverse(Rinv);
       
/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
		//	 printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));

               printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
                }
                printf("\nline\n");
        }
*/

		
}





int main(int argc, char **argv)
{
	int i,j;
	int nxy=NX*NY;
	double complex R[NX][NY],Rinv[nxy][nxy];
	char *string_list[2048];
	 CManager cm;
        EVstone stone;
        EVsource source;
        attr_list contact_list;
        EVstone remote_stone;
	struct timespec start, end, elapsed;
	
	if (sscanf(argv[1], "%d:%s", &remote_stone, &string_list[0]) != 2) {

        printf("Bad arguments \"%s\"\n", argv[1]);
        exit(0);
 	   }
	
	//int addr;
	//(void) inet_aton(DEST_IP, (struct in_addr *)&addr);
        cm=CManager_create();
        CMlisten(cm);
        stone=EValloc_stone(cm);
	contact_list=create_attr_list();
	contact_list=attr_list_from_string(string_list);
       	EVassoc_bridge_action(cm,stone,contact_list,remote_stone);
        source=EVcreate_submit_handle(cm,stone,inverse_format_list);
	inverse inv;

	clock_gettime(CLOCK_MONOTONIC,&start);
		
	
	calc_covariance(&Rinv,&R);


	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		inv.inv_data[i][j].real=creal(Rinv[i][j]);
		inv.inv_data[i][j].imag=cimag(Rinv[i][j]);
		}
	}
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		inv.raw_data[i][j].real=creal(R[i][j]);
		inv.raw_data[i][j].imag=cimag(R[i][j]);
		}
	}
	
	gettimeofday(&sent,NULL);
	EVsubmit(source,&inv,NULL);
	clock_gettime(CLOCK_MONOTONIC,&end);
	elapsed.tv_sec=(end.tv_sec)-(start.tv_sec);
	elapsed.tv_nsec=(end.tv_nsec)-(start.tv_nsec);
	printf("\n Cnode data sent at %ld usec\n", sent.tv_usec + (sent.tv_sec * 1000000));
	printf("\nCovariance Elapsed Time %f nsec\n",((double)elapsed.tv_nsec));
	
}	

