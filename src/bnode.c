#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
#include<math.h>
#include<time.h>
#include "../include/init.h"
#include "../../include/evpath.h"

#define OVERSAMP 8
#define NX 16
#define NY 16
#define PI 3.14159
#define NXY NX*NY

struct timeval received;
double complex Rinv[NXY][NXY];
double complex R[NX][NY];

typedef struct {
 double real;
 double imag;
}comp;

typedef struct _inverse{
    comp inv_data[NX*NY][NX*NY];
    comp raw_data[NX][NY];
} inverse, *inverse_ptr;
static FMField compfield_list[] =
{
    {"real", "float", sizeof(float), FMOffset(comp*, real)},
   {"imag", "float", sizeof(float), FMOffset(comp*, imag)},
    {NULL, NULL, 0, 0}
};

static FMField inverse_field_list[] =
{
  {"inv_data", "comp[256][256]", sizeof(comp), FMOffset(inverse*, inv_data)},
 {"raw_data", "comp[16][16]", sizeof(comp), FMOffset(inverse*, raw_data)},
    {NULL, NULL, 0, 0}
};


static FMStructDescRec inverse_format_list[] =
{
    {"inverse", inverse_field_list, sizeof(inverse), NULL},
   {"comp", compfield_list, sizeof(comp), NULL},
    {NULL, NULL}
};

void vectorize(double complex *jnv, double complex jn[NX][NY])
{

        int i,j,k=0;
        for(i=0;i<NX;++i)
        {
          for(j=0;j<NY;++j)
          {
                jnv[k++]=jn[j][i];

          }
        }
}


static int
inverse_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	
	//timestamp

    inverse_ptr event = vevent;
	gettimeofday(&received,NULL);

   int i,j;
    for(i=0;i<NXY;++i)
	{
	for(j=0;j<NXY;++j)	
	{
	Rinv[i][j]=(event->inv_data[i][j].real)+(I*(event->inv_data[i][j].imag));
    	}
	}
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		R[i][j]=(event->raw_data[i][j].real)+(I*(event->raw_data[i][j].imag));
		}
	}
	printf("\nBnode received data at %ld usec \n", (received.tv_usec + (received.tv_sec * 1000000)));
	
/*	 for(i=0;i<NX;++i)
        {
                for(j=0;j<NY;++j)
                {
		printf("%f+i%f\t",R[i][j]);
		}
		printf("\nline\n");
	}*/
}

void oversampling(double complex *sceneOpt,double complex *sceneMat)
{
        int numxbeams=OVERSAMP*NX+1, numybeams=OVERSAMP*NY+1,nxy=NX*NY,p,q;
        double azs[numxbeams],els[numybeams];
	double az,el;
	int i=0;
	double complex wopt[nxy],wopt1[nxy],wmat[nxy],wmat1[nxy];
	double complex sjn[nxy];
	double ixs[numxbeams],iys[numybeams];
	double complex optval,matchval;
	int j=0;
	double complex sv_vec[nxy];
	double wopt_sum,wmatch_sum,sum=0,test_sum=0+0*I;
	double complex sv[NX][NY];
	int indi,indj;
//	struct timespec start, end, elapsed;
/*	printf("R and sjn\n");
	for(i=0;i<NX;++i)	
	{
		for(j=0;j<NY;++j)
		printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
	}*/
	for(i=0;i<nxy;++i)
	{
	wopt[i]=0;
	wmat[i]=0;
	}
	 vectorize(&sjn,R);
//	printf("sjn\n");
/*	for(i=0;i<nxy;++i)
	{
	printf("%f\t",creal(sjn[i]));
	test_sum+=sjn[i];	
	}
	printf("\nline\nsum=%f+i%f\n",creal(test_sum),cimag(test_sum));*/
	for(i=0;i<numxbeams;++i)
	ixs[i]=i+1;
	for(j=0;j<numybeams;++j)
        iys[j]=j+1;
	for(i=0;i<numxbeams;++i)
	azs[i]=(((ixs[i]-1)/128)-0.5)*180;
       for(j=0;j<numybeams;++j)
        els[j]=(((iys[j]-1)/128)-0.5)*180;
	
	for(i=0;i<numxbeams;++i)
	{
		//indi=ixs[i];
		az=azs[i];
		for(j=0;j<numybeams;++j)
		{
			//indj=iys[j];
			el=els[j];
			//printf("\n%f %f\n",az,el);
			signalcomp(&sv,1,az,el);
			for(p=0;p<nxy;++p)
			sv_vec[p]=0;
			vectorize(&sv_vec,sv);
			for(p=0;p<nxy;++p)
			{
         	              wopt[p]=0+I*0;
			      wmat[p]=0+I*0;
			}
			for(p=0;p<nxy;++p)
			{
				for(q=0;q<nxy;++q)
				{
				wopt[p]+=Rinv[p][q]*sv_vec[q];
				wmat[p]=sv_vec[p];
				}
			}
	/*		printf("Rinv\n");
			 for(p=0;p<nxy;++p)
                        {
				for(q=0;q<nxy;++q)
			     {
				
                              printf("%f+i%f\t",Rinv[p][q]);
				}
				printf("\nline\n");
                        }*/

//		printf("sv\n");
//		for(p=0;p<nxy;++p)
//		{
			// for(q=0;q<nxy;++q)
//			printf("%f+i%f\t",sv_vec[p]);
			//	printf("\nline\n");
//		}	
			wopt_sum=0;
			wmatch_sum=0;
			for(p=0;p<nxy;++p)
			{
			wopt_sum+=pow(cabs(wopt[p]),2);		
			wmatch_sum+=pow(cabs(wmat[p]),2);
			}
			//should sum be current to every iteration?
			for(p=0;p<nxy;++p)
			{
			wopt[p]=(wopt[p]/sqrt(wopt_sum))/sqrt(nxy);
			wmat[p]=(wmat[p]/sqrt(wmatch_sum))/sqrt(nxy);
			}
			 /*printf("wopt\n");
                         for(p=0;p<nxy;++p)
                        {
                              printf("%f+i%f\t",wopt[p]);
                        }*/
		

	
			/*for(p=0;p<nxy;++p)
	              	{
	                      printf("%f+i%f\t",creal(wmat[p]),cimag(wmat[p]));
	        	} */  
			
			for(p=0;p<nxy;++p)
			{
			// for(q=0;q<nxy;++q)
			//	{
					*(sceneOpt+(j*numybeams+i))+=conj(wopt[p])*(sjn[p]);
				//	printf("%f+i%f",creal(*(sceneOpt+(i*numybeams+j))),cimag(*(sceneOpt+(i*numybeams+j))));	
					*(sceneMat+(j*numybeams+i))+=conj(wmat[p])*(sjn[p]);
					//sum+=sjn[p];
				//	   printf("%f\t",creal(conj(wmat[q])));
                                       
			//	}
			}
			/*printf("\nline\n");
		printf("%f+i%f",creal(*(sceneOpt+(i*numybeams+j))),cimag(*(sceneOpt+(i*numybeams+j))));*/


			
		}
	}
/*	for(p=0;p<nxy;++p)
         {
                 // for(q=0;q<nxy;++q)
                 printf("%f+i%f\t",creal(sv_vec[p]),cimag(sv_vec[p]));
                // printf("\nline\n");
        }   */    
                //
/*	for(i=0;i<numxbeams;++i)
	{
		//indi=ixs[i];
		for(j=0;j<numybeams;++j)
		{
			//indj=iys[j];
		  printf("%f+i%f",creal(*(sceneMat+(i*numybeams+j))),cimag(*(sceneMat+(i*numybeams+j))));
		}
		printf("\nline\n");
	}
*/	

}

void main()
{
	int i,j;
	double complex sceneOpt[NX*OVERSAMP+1][NY*OVERSAMP+1],sceneMat[NX*OVERSAMP+1][NY*OVERSAMP+1];
	double optAbs[NX*OVERSAMP+1][NY*OVERSAMP+1],matAbs[NX*OVERSAMP+1][NY*OVERSAMP+1];

	// struct timespec start, end, elapsed;
	struct timeval start,end,elapsed;

	for(i=0;i<NX*OVERSAMP+1;++i)
	{
		for(j=0;j<NY*OVERSAMP+1;++j)
		{
		sceneOpt[i][j]=0;
		sceneMat[i][j]=0;
		}
	}
	CManager cm;
	EVstone stone;
	char *string_list;
	cm = CManager_create();
	CMlisten(cm);
	stone=EValloc_stone(cm);
		
	EVassoc_terminal_action(cm,stone,inverse_format_list,inverse_handler,NULL);
	string_list = attr_list_to_string(CMget_contact_list(cm));
	printf("%d:%s",stone,string_list);


	gettimeofday(&start,NULL);
	//clock_gettime(CLOCK_MONOTONIC,&start);	

	CMsleep(cm,35);
//	sleep(50);
	oversampling(&sceneOpt,&sceneMat);
	
	for(i=0;i<OVERSAMP*NX+1;++i)
	{
		for(j=0;j<OVERSAMP*NY+1;++j)
		{
		optAbs[i][j]=cabs(sceneOpt[i][j]);
		matAbs[i][j]=cabs(sceneMat[i][j]);
		}
	}	
	
	gettimeofday(&end,NULL);
        elapsed.tv_sec=(end.tv_sec)-(start.tv_sec);
        elapsed.tv_usec=(end.tv_usec)-(start.tv_usec);
        printf("\nBnode Elapsed Time %f usec\n",(((double)elapsed.tv_usec) + ((double)elapsed.tv_sec*1000000)));
	

	FILE *output,*outputweiner;

	output=fopen("output.txt","w");
	outputweiner=fopen("outputweiner.txt","w");
	for(i=0;i<129;++i)
	{
		for(j=0;j<129;++j)
		{
			fprintf(output,"%f\t",matAbs[i][j]);
			fprintf(outputweiner,"%f\t",optAbs[i][j]);
		}
		fprintf(output,"\n");
		fprintf(outputweiner,"\n");
	}
	fclose(output);
	fclose(outputweiner);


}

