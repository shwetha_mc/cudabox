#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
#include<string.h>
#include<arpa/inet.h>
#include<time.h>
#include "emmintrin.h"
#include "../include/setup.h"
#include "../../include/evpath.h"
#include "../../include/atl.h"
#define NX 16
#define NY 16
#define SIGRANGEBIN 50
//#define RANGEBINS DEFAULT 1000
#define NXY NX*NY
#define DEST_PORT 26068 //26067 26068
#define DEST_IP "10.255.255.188"

struct timeval sent;
int rangebins;
int nxy=NX*NY;
typedef struct {
 double real;
 double imag;
}comp;

typedef struct _inverse {
    comp inv_data[NXY][NXY];
    comp raw_data[NX][NY];
} inverse, *inverse_ptr;
static FMField compfield_list[] =
{
    {"real", "float", sizeof(float), FMOffset(comp*, real)},
   {"imag", "float", sizeof(float), FMOffset(comp*, imag)},
    {NULL, NULL, 0, 0}
};

static FMField inverse_field_list[] =
{
  {"inv_data", "comp[256][256]", sizeof(comp), FMOffset(inverse*, inv_data)},
{"raw_data", "comp[16][16]", sizeof(comp), FMOffset(inverse*, raw_data)},
    {NULL, NULL, 0, 0}
};


static FMStructDescRec inverse_format_list[] =
{
    {"inverse", inverse_field_list, sizeof(inverse), NULL},
   {"comp", compfield_list, sizeof(comp), NULL},
    {NULL, NULL}
};

double complex mul_comp(double complex a, double complex b)
{
	return(((creal(a)*creal(b))-(cimag(a)*cimag(b)))+I*((creal(a)*cimag(b))+(cimag(a)*creal(b))));
}
double complex div_comp(double complex a, double complex b)
{
	 return((((creal(a)*creal(b))-(cimag(a)*cimag(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b))))+I*(((creal(a)*cimag(b))+(cimag(a)*creal(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b)))));
}



void vectorize(double complex *jnv, double complex jn[NX][NY])
{

	int i,j,k=0;
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
	  {
		//if(k<NXY)
		//{
		*(jnv+k)=jn[j][i];
//(round(creal(jn[i][j])*10000)/10000)+I*((round(cimag(jn[i][j])*10000)/10000));
		++k;
	//	}
		
	  }
	}
}

/*void calc_inverse(double complex *Rinv)
{
	
	printf("Passed %s %d\n", __func__,__LINE__ );

	//int nxy=2;
//	int nxy=NX*NY;
        int i,j,k,temp;
	printf("Passed %s %d\n", __func__,__LINE__ );
	nxy=NX*NY;
  printf("Passed %s %d\n", __func__,__LINE__ );

        double complex iden[nxy][nxy];
 printf("Passed %s %d\n", __func__,__LINE__ );
	double complex arr[nxy][nxy];
     //   double arr1[nxy][nxy],arr2[nxy][nxy];
	printf("Passed %s %d\n", __func__,__LINE__ );
        double complex r,swaptemp,factor;
printf("Passed %s %d\n", __func__,__LINE__ );


for(i=0;i<nxy;i++)
        {
                for(j=0;j<nxy;++j)
                {
                arr[i][j]=(*(Rinv+(i*nxy)+j));
                //arr2[i][j]=cimag(*(Rinv+(i*nxy)+j));
                }
        }

*/

       /*for(i=0;i<nxy;i++)
        {
                for(j=0;j<nxy;++j)
		{
                arr1[i][j]=creal(*(Rinv+(i*nxy)+j));
		arr2[i][j]=cimag(*(Rinv+(i*nxy)+j));
		}
        }*/
/*printf("Passed %s %d\n", __func__,__LINE__ );

*/
      /*Identity matrix*/
  /*      for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                        iden[i][j]=(i==j)?(1+0*I):(0+0*I);
        }
*/	
/*	for(i=0;i<nxy;++i)
	{	
		for(j=0;j<nxy;++j)
		arr[i][j]=arr1[i][j];//+I*arr2[i][j];
	}
*/
	/*augmenting base matrix with identity*/

  /*for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        arr[i][j+nxy]=iden[i][j];
                }
        }







*/

        /*Gaussian decomposition*/
/*	for(k=0;k<nxy;++k)
        {
        factor=((arr[k][k]));
        for(i=0;i<2*nxy;++i)
        {
                arr[k][i]=(arr[k][i]/factor);
        }
        for(i=0;i<nxy;++i)
        {
                if(i!=k)
                {
                factor=((arr[i][k]));
                for(j=0;j<2*nxy;++j)
                {
                        arr[i][j]=arr[i][j]-(arr[k][j]*factor);

                }
                }
        }
        }

printf("Passed %s %d\n", __func__,__LINE__ );
*/

//	printf("\nends\n");
	 /*assignment*/
  /*      for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        *(Rinv+(i*nxy+j))=arr[i][j+nxy];
                }
        }
printf("Passed %s %d\n", __func__,__LINE__ );	
*/	/* for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
		printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
		}
		printf("\nline\n");
	}
		
	
	
}*/

void calc_covariance(double complex *Rinv, double complex *raw_data)
{
	int m,i,j,k=0,nxy=NX*NY;
	double complex val=0;
	double complex jam[NX][NY],sig[NX][NY],jn[NX][NY],jncopy[NX][NY],jnv[nxy],R[nxy][nxy];
	int p,q;
	double complex inter[nxy][nxy];
	double Rreal1[nxy][nxy];
	double Rimag1[nxy][nxy];
	double *Rreal , *Rimag;
	double jnvreal1[nxy];
	double jnvimag1[nxy];
	double interreal1[nxy][nxy];
	double interimag1[nxy][nxy];
	double *jnvreal, *jnvimag;
	double *interreal, *interimag;
	
	interreal=&interreal1;
	interimag=&interimag1;
	for(i=0;i<nxy;++i)
	{
  		for(j=0;j<nxy;++j)
  		{
        	Rreal1[i][j]=0;
        	Rimag1[i][j]=0;
  		}
	}
	Rreal=&Rreal1;
	Rimag=&Rimag1;
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
			sig[i][j]=0;
                        jam[i][j]=0;
                        jn[i][j]=0;
			*(raw_data+(i*NY)+j)=0;


		}
	}
	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
			*(Rinv+(i*nxy)+j)=0;
			R[i][j]=0;
		}
	}
	get_jam_signal(&jam);
	get_tgt_signal(&sig);
	//get_jn(&jn,&jam);	
	
/*	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		jn[i][j]=jam[i][j];
		}
	}*/
	
	if(!rangebins)
	{
	FILE *rngbin;
	rngbin=fopen("../rangebins.txt","r");
	fscanf(rngbin,"%d \n",&rangebins);
	fclose(rngbin);
	}
	printf("Rangebins set : %d\n", rangebins);
//	for(m=0;m<rangebins;++m)
	for(m=0;m<1;++m)
	{
//	int w,v=0;
         get_jn(&jn,&jam);
	 if((m+1)==SIGRANGEBIN)
	 {
		for(i=0;i<NX;++i)
		{
			for(j=0;j<NY;++j)
			{
			jn[i][j]+=sig[i][j];
			*(raw_data+(i*NY+j))=jn[i][j];
			}
		}
	 }
//		printf("jnv\n");	
/*	for(i=0;i<NX;++i)
	{
	for(j=0;j<NY;++j)
	{
		jncopy[i][j]=jn[j][i];
	}
	}*/
	 vectorize(&jnv,jn);
	
	/*printf("jn\n");
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		printf("%f+i%f\t",creal(jn[i][j]),cimag(jn[i][j]));
		printf("\nline\n");
	}*/
	
/********* Vectorize*********/

	__m128d zerov = _mm_setzero_pd();
	
	for(i=0;i<nxy;++i)
	{
	jnvreal1[i]=creal(jnv[i]);
	jnvimag1[i]=cimag(jnv[i]);
	}
	jnvreal=&jnvreal1;
	jnvimag=&jnvimag1;
//	printf("Passed %s %d\n", __func__,__LINE__ );

	i=0,j=0;
//	__m128d zerov=_mm_setzero_pd();
	while((i+2)<nxy)
	{

	__m128d v_interreal=_mm_setzero_pd();
        __m128d v_inter2real=_mm_setzero_pd();
        __m128d v_interimag=_mm_setzero_pd();
        __m128d v_inter2imag=_mm_setzero_pd();
		j=0;

		while((j+4)<nxy)
		{

	//	 printf("Passed %s %d\n", __func__,__LINE__ );
		

		__m128d v_jnvreal=_mm_loadu_pd(jnvreal+i);
		__m128d v_jnvconjreal=_mm_loadu_pd(jnvreal+j);
		__m128d v_jnvconj2real=_mm_loadu_pd(jnvreal+j+2);
		
		__m128d v_jnvimag=_mm_loadu_pd(jnvimag+i);
		__m128d v_jnvconjimag=_mm_loadu_pd(jnvimag+j);
		__m128d v_jnvconj2imag=_mm_loadu_pd(jnvimag+j+2);

		
		v_jnvconjimag=_mm_sub_pd(zerov,v_jnvconjimag);
		v_jnvconj2imag=_mm_sub_pd(zerov,v_jnvconj2imag);
		
	//	printf("Passed %s %d\n", __func__,__LINE__ );
	
		v_interreal=_mm_add_pd(v_interreal,_mm_sub_pd(_mm_mul_pd(v_jnvreal,v_jnvconjreal),_mm_mul_pd(v_jnvimag,v_jnvconjimag)));
		v_interimag=_mm_add_pd(v_interimag,_mm_add_pd(_mm_mul_pd(v_jnvreal,v_jnvconjimag),_mm_mul_pd(v_jnvimag,v_jnvconjreal)));
		

		v_inter2real=_mm_add_pd(v_inter2real,_mm_sub_pd(_mm_mul_pd(v_jnvreal,v_jnvconj2real),_mm_mul_pd(v_jnvimag,v_jnvconj2imag)));
                v_inter2imag=_mm_add_pd(v_inter2imag,_mm_add_pd(_mm_mul_pd(v_jnvreal,v_jnvconj2imag),_mm_mul_pd(v_jnvimag,v_jnvconj2real)));

		
			

		 _mm_store_pd((interreal+(i*nxy)+j),v_interreal);
//		_mm_storeh_pd((double *)(&interreal[i+1][j+1]),_mm_unpackhi_pd(v_interreal,zerov));
		_mm_store_pd((interreal+(i*nxy)+j+2),v_inter2real);
//		 _mm_storeh_pd((double *)(&interreal[i+1][j+3]),_mm_unpackhi_pd(v_inter2real,zerov));
		
		_mm_store_pd((interimag+(i*nxy)+j),v_interimag);
//		_mm_storeh_pd((double *)(&interimag[i+1][j+1]),_mm_unpackhi_pd(v_interimag,zerov));

		_mm_store_pd((interimag+(i*nxy)+j+2),v_inter2imag);
//		 _mm_storeh_pd((double *)(&interimag[i+1][j+3]),_mm_unpackhi_pd(v_inter2imag,zerov));
		
		//_mm_storeu_pd((double *)(interreal+i+j),jnvreal+i);
		
	
		j+=4;
		}
		
//	printf("Passed %s %d\n", __func__,__LINE__ );

	       i++;
	}
	
///
/*	printf("post first vector");
	 for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                printf("%f+i%f\t",interreal1[i][j],interimag1[i][j]);
                }
                printf("\nline\n");
        }
*/


////
	i=0,j=0;
	
 	while((i)<nxy)
	{
		j=0;
		while((j+4)<nxy)
		{
		
			//printf("Passed %s %d\n", __func__,__LINE__ );

			__m128d vinterreal=_mm_loadu_pd(interreal+(i*nxy)+j);
			__m128d vinter2real=_mm_loadu_pd(interreal+(i*nxy)+j+1);
			__m128d vinter3real=_mm_loadu_pd(interreal+(i*nxy)+j+2);
			__m128d vinter4real=_mm_loadu_pd(interreal+(i*nxy)+j+3);
			
			__m128d vinterimag=_mm_loadu_pd(interimag+(i*nxy)+j);
                        __m128d vinter2imag=_mm_loadu_pd(interimag+(i*nxy)+j+1);
                        __m128d vinter3imag=_mm_loadu_pd(interimag+(i*nxy)+j+2);
                        __m128d vinter4imag=_mm_loadu_pd(interimag+(i*nxy)+j+3);

//			printf("Passed %s %d\n", __func__,__LINE__ );
	
			__m128d vRreal=_mm_loadu_pd(Rreal+(i*nxy)+j);
			__m128d vR2real=_mm_loadu_pd(Rreal+(i*nxy)+j+1);
			__m128d vR3real=_mm_loadu_pd(Rreal+(i*nxy)+j+2);
			__m128d vR4real=_mm_loadu_pd(Rreal+(i*nxy)+j+3);


			 __m128d vRimag=_mm_loadu_pd(Rimag+(i*nxy)+j);
                        __m128d vR2imag=_mm_loadu_pd(Rimag+(i*nxy)+j+1);
                        __m128d vR3imag=_mm_loadu_pd(Rimag+(i*nxy)+j+2);
                        __m128d vR4imag=_mm_loadu_pd(Rimag+(i*nxy)+j+3);
			
//			 printf("Passed %s %d\n", __func__,__LINE__ );
	

	
			vRreal=_mm_add_pd(vRreal,vinterreal);
			vR2real=_mm_add_pd(vR2real,vinter2real);
			vR3real=_mm_add_pd(vR3real,vinter3real);
			vR4real=_mm_add_pd(vR4real,vinter4real);


			 vRimag=_mm_add_pd(vRimag,vinterimag);
                        vR2imag=_mm_add_pd(vR2imag,vinter2imag);
                        vR3imag=_mm_add_pd(vR3imag,vinter3imag);
                        vR4imag=_mm_add_pd(vR4imag,vinter4imag);

//			 printf("Passed %s %d\n", __func__,__LINE__ );

	
			_mm_storeu_pd(Rreal+(i*nxy+j),vRreal);
			_mm_storeu_pd(Rreal+(i*nxy+j+1),vR2real);
			_mm_storeu_pd(Rreal+(i*nxy+j+2),vR3real);
			_mm_storeu_pd(Rreal+(i*nxy+j+3),vR4real);
			
			 _mm_storeu_pd(Rimag+(i*nxy+j),vRimag);
                        _mm_storeu_pd(Rimag+(i*nxy+j+1),vR2imag);
                        _mm_storeu_pd(Rimag+(i*nxy+j+2),vR3imag);
                        _mm_storeu_pd(Rimag+(i*nxy+j+3),vR4imag);

//			 printf("Passed %s %d\n", __func__,__LINE__ );


			j+=4;	
		}
		i++;
	}
	
	
	
//printf("Passed %s %d\n", __func__,__LINE__ );




/******Vecends*******/



/***Uncomment later for unvectorized code
 
	for(p=0;p<nxy;++p)
	{
		for(q=0;q<nxy;++q)
		inter[p][q]=0;
	}

         for(i=0;i<nxy;++i)
	 {
		for(j=0;j<nxy;++j)
		{
		inter[i][j]=jnv[i]*(conj(jnv[j]));
		}
	}
	
	 for(p=0;p<nxy;++p)
	 {
		for(q=0;q<nxy;++q)
		{
		R[p][q]+=inter[p][q];
		}
	 }

**********************Upto here*/

/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
                printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
                }
                printf("\nline\n");
        }

*/	
	}
//	printf("Passed %s %d\n", __func__,__LINE__ );

 	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		*(Rinv+(i*nxy)+j)=Rreal1[i][j]+I*Rimag1[i][j];
		}
	}
//	printf("Passed %s %d\n", __func__,__LINE__ );
/*	 for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
		{
		printf("%f+i%f\t",creal(*(Rinv+i*NXY+j)),cimag(*(Rinv+i*NXY+j)));
		}
		printf("\nline\n");
	}
*/
	calc_inverse(Rinv);

//	printf(" \ncovariance\n  ");       
/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
		//	 printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));

               printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
                }
                printf("\nline\n");
        }

*/
		
}





int main(int argc, char **argv)
{
	int i,j;
//	int nxy=NX*NY;
	double complex R[NX][NY],Rinv[nxy][nxy];
	char *string_list[2048];
	 CManager cm;
        EVstone stone;
        EVsource source;
        attr_list contact_list;
        EVstone remote_stone;
	struct timeval start, end, elapsed;
	
	if (sscanf(argv[1], "%d:%s", &remote_stone, &string_list[0]) != 2) {

        printf("Bad arguments \"%s\"\n", argv[1]);
        exit(0);
 	   }
	
	//int addr;
	//(void) inet_aton(DEST_IP, (struct in_addr *)&addr);
        cm=CManager_create();
        CMlisten(cm);
        stone=EValloc_stone(cm);
	contact_list=create_attr_list();
	contact_list=attr_list_from_string(string_list);
       	EVassoc_bridge_action(cm,stone,contact_list,remote_stone);
        source=EVcreate_submit_handle(cm,stone,inverse_format_list);
	inverse inv;

	gettimeofday(&start,NULL);
		
	
	calc_covariance(&Rinv,&R);


	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		inv.inv_data[i][j].real=creal(Rinv[i][j]);
		inv.inv_data[i][j].imag=cimag(Rinv[i][j]);
		}
	}
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		inv.raw_data[i][j].real=creal(R[i][j]);
		inv.raw_data[i][j].imag=cimag(R[i][j]);
		}
	}
	
	gettimeofday(&sent,NULL);
	EVsubmit(source,&inv,NULL);
	gettimeofday(&end,NULL);
	elapsed.tv_sec=(end.tv_sec)-(start.tv_sec);
	elapsed.tv_usec=(end.tv_usec)-(start.tv_usec);
	printf("\n Cnode data sent at %ld usec\n", sent.tv_usec + (sent.tv_sec * 1000000));
	printf("\nCovariance Elapsed Time %f usec\n",(((double)elapsed.tv_usec)+((double)elapsed.tv_sec*1000000)));
	
}	

